package main

import (
	"context"
	"fmt"
	"gitlab.com/ilya.sheidin/git-grpc-blog-server/models"
	blogpb "gitlab.com/ilya.sheidin/git-grpc-blog-server/proto"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/reflection"
	"google.golang.org/grpc/status"
	"log"
	"net"
	"os"
	"os/signal"
	"time"
)

var collection *mongo.Collection

type ServiceServer struct {
}

func (s *ServiceServer) ListBlog(req *blogpb.ListBlogRequest, stream blogpb.BlogService_ListBlogServer) error {
	cur, err := collection.Find(context.Background(), bson.M{})
	if err != nil {
		fmt.Println(err)
		return status.Errorf(codes.Internal, fmt.Sprintf("Unknown read list error:%v", err))
	}
	defer cur.Close(context.Background())
	for cur.Next(context.Background()) {
		bi := &models.BlogItem{}
		err = cur.Decode(bi)
		if err != nil {
			fmt.Println(err)
			return status.Errorf(codes.Internal, fmt.Sprintf("Error while decoding data:%v", err))
		}
		err = stream.Send(&blogpb.ListBlogResponse{
			Blog: dataToBlockPb(*bi),
		})
		if err != nil {
			fmt.Println(err)
			return status.Errorf(codes.Internal, "Error while sending stream")
		}
	}
	return nil
}

func (s *ServiceServer) DeleteBlog(ctx context.Context, req *blogpb.DeleteBlogRequest) (*blogpb.DeleteBlogResponse, error) {
	fmt.Println("Delete method invoked")
	bi := models.BlogItem{}
	data, err := bi.FindById(collection, req.BlogId)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	_, err = collection.DeleteOne(context.Background(), bson.M{"_id": data.ID})
	if err != nil {
		fmt.Println(err)
		return nil, status.Errorf(codes.Internal, "Failed to update records")
	}
	return &blogpb.DeleteBlogResponse{
		Blog: dataToBlockPb(bi),
	}, nil
}

func (*ServiceServer) UpdateBlog(ctx context.Context, req *blogpb.UpdateBlogRequest) (*blogpb.UpdateBlogResponse, error) {
	fmt.Println("Update method invoked")

	blog := req.GetBlog()
	bi := models.BlogItem{}
	fmt.Println("Updating id:", blog.GetId())
	data, err := bi.FindById(collection, blog.GetId())
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	nItem := models.BlogItem{}
	nItem.ID = data.ID
	nItem.Content = blog.Content
	nItem.Title = blog.Title
	nItem.AuthorID = blog.AuthorId
	fmt.Println("Original:", data)
	fmt.Println("Replacement:", nItem)
	_, err = collection.ReplaceOne(context.Background(), bson.M{"_id": data.ID}, nItem)
	if err != nil {
		fmt.Println(err)
		return nil, status.Errorf(codes.Internal, "Failed to update records")
	}
	return &blogpb.UpdateBlogResponse{
		Blog: dataToBlockPb(nItem),
	}, nil
}

func (s ServiceServer) ReadBlog(ctx context.Context, req *blogpb.ReadBlogRequest) (*blogpb.ReadBlogResponse, error) {
	id := req.GetBlogId()

	bi := models.BlogItem{}
	fmt.Println("Reading id:", id)
	data, err := bi.FindById(collection, id)
	if err != nil {
		log.Fatalf("Failed to read data: %v", err)
		return nil, err
	}

	return &blogpb.ReadBlogResponse{
		Blog: dataToBlockPb(data),
	}, nil
}

func dataToBlockPb(data models.BlogItem) *blogpb.Blog {
	return &blogpb.Blog{
		Id:       primitive.ObjectID.Hex(data.ID),
		AuthorId: data.AuthorID,
		Title:    data.Title,
		Content:  data.Content,
	}
}

func (s ServiceServer) CreateBlog(ctx context.Context, req *blogpb.CreateBlogRequest) (*blogpb.CreateBlogResponse, error) {
	blogReq := req.GetBlog()
	/*Map data*/
	data := models.BlogItem{
		AuthorID: blogReq.GetAuthorId(),
		Content:  blogReq.GetContent(),
		Title:    blogReq.GetTitle(),
	}
	res, err := collection.InsertOne(context.Background(), data)
	if err != nil {
		return nil, status.Errorf(codes.Internal, fmt.Sprintf("Failed to insert record: %v", err))
	}
	/*Cast OID*/
	oid, ok := res.InsertedID.(primitive.ObjectID)
	if !ok {
		return nil, status.Errorf(codes.Internal, fmt.Sprintf("Failed to obtain id: %v", err))
	}
	fmt.Println("New blog created")
	return &blogpb.CreateBlogResponse{
		Blog: &blogpb.Blog{
			Id:       oid.Hex(),
			AuthorId: blogReq.GetAuthorId(),
			Title:    blogReq.GetContent(),
			Content:  blogReq.GetTitle(),
		},
	}, nil
}

func main() {
	dbClient := connectToDb()
	setConnectionToTheServer(dbClient)
}

func connectToDb() *mongo.Client {
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(fmt.Sprintf("mongodb://%v:%v@%v:%v",
		os.Getenv("DB_USER"), os.Getenv("DB_PASSWORD"), os.Getenv("DB_HOST"), os.Getenv("DB_PORT"))))
	if err != nil {
		log.Fatalf("Failed to connect to DB: %v", err)
	}
	collection = client.Database(os.Getenv("DB_DATABASE")).Collection("blog")
	return client
}

func setConnectionToTheServer(dbClient *mongo.Client) {
	// if we crash the go code, we get the filename
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	cd := fmt.Sprintf("%v:%v", os.Getenv("SERVER_IP"), os.Getenv("PORT"))
	fmt.Println("Ready for connection on:", cd)
	lis, err := net.Listen("tcp", cd)
	if err != nil {
		log.Fatalf("Failed to create server %v", err)
	}
	s := grpc.NewServer()
	blogpb.RegisterBlogServiceServer(s, &ServiceServer{})
	reflection.Register(s)
	go func() {
		fmt.Println("Starting server")
		if err := s.Serve(lis); err != nil {
			log.Fatalf("Failed to listen %v", err)
		}
	}()

	/*Wait for Control C to exit*/
	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt)

	// Block until a signal received
	<-ch
	fmt.Println("Stopping the server")
	s.Stop()
	fmt.Println("Closing the listener")
	err = lis.Close()
	fmt.Println("Close DB server")
	if err != nil {
		log.Fatalf("Failed to closr listener: %v", err)
	}
	err = dbClient.Disconnect(context.TODO())
	if err != nil {
		log.Fatalf("Failed to disconnect from the DB: %v", err)
	}

}
