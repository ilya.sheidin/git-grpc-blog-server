#!/usr/bin/env bash
go mod vendor && protoc ./proto/blog.proto --go_out=plugins=grpc:. && CompileDaemon --build="go build server.go" --command=./server