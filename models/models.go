package models


type ModelError struct {
	message string
}

func (e *ModelError) Error() string {
	return e.message
}