package models

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)



type BlogItem struct {
	ID       primitive.ObjectID `bson:"_id,omitempty"`
	AuthorID string             `bson:"author_id"`
	Content  string             `bson:"content"`
	Title    string             `bson:"title"`
}

func (bi *BlogItem) FindById(collection *mongo.Collection, id string) (BlogItem, error) {
	fmt.Println(id)
	oid, err := primitive.ObjectIDFromHex(id)
	if err != nil {

		return *bi, status.Errorf(codes.InvalidArgument, fmt.Sprintf("Failed to parse id:%v", id))
	}
	fmt.Println(oid)
	filter := bson.M{"_id": oid}
	res := collection.FindOne(context.Background(), filter)
	if err = res.Decode(bi); err != nil {
		fmt.Println(fmt.Sprintf("Record with id:%v not found", id))
		return *bi, status.Errorf(codes.NotFound, fmt.Sprintf("Record with id:%v not found", id))
	}
	return *bi, nil
}
